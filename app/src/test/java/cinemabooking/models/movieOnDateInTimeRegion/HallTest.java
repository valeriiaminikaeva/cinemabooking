/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package cinemabooking.models.movieOnDateInTimeRegion;

import cinemabooking.movieOnDateInTimeRegion.Hall;
import cinemabooking.movieOnDateInTimeRegion.Seat;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class HallTest {
        @Test void toStringCheck() {
            Seat a = new Seat(false, 100);
            Seat b = new Seat(true, 100);
            Seat c = new Seat(false, 100);
            Seat d = new Seat(true, 100);
            Seat e = new Seat(true, 100);
            ArrayList<Seat> seats = new ArrayList<>();
            seats.add(a);
            seats.add(b);
            seats.add(c);
            seats.add(d);
            seats.add(e);
            Hall hall = new Hall("Gold", seats);
            assertEquals(hall.toString(),"Hall name: Gold\n" + "Size: 5\n" + "Free places: 2");
        }

}
