package cinemabooking.models.contextRegion;

import cinemabooking.movieOnDateInTimeRegion;
import cinemabooking.clientRegion;
import cinemabooking.contextRegion;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    @Test void checkAll() {
        Movie movie = new movie("Что то о любви" , "История любви" , "Драма" , 18 , 90 , 600.0);
        Client client = new Client(1, "", "2022-05-05 00:00:00", "", "", true);
        ArrayList<Discount> discounts = new ArrayList<Discount>;
        Context context = new Context(client, movie, discounts);
        Calculator calculator = new Calculator(context);
        calculator.addDiscount();
        calculator.priceWithoutDiscount();
        calculator.applyDiscount();
        assertEquals(calculator.applyDiscount(),"480.0");
    }

}