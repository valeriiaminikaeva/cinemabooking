package cinemabooking.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ArrayList;


public class ConnectionDB {

    /**
     * логин для коннекта к БД
     */
    private static String CONNECTION_USER = "postgres";

    /**
     * пароль для коннекта к БД
     */
    private static String CONNECTION_PASSWORD = "123";

    /**
     * URL для коннекта к БД
     */
    private static String CONNECTION_URL = "jdbc:postgresql://10.82.66.162:999/Cinemabooking";

    /**
     * синглтон Connection
     */
    private static Connection conn;

    /**
     * метод коннекта к БД
     */
    private static Connection createConn() {
        Properties connectionProps = new Properties();
        connectionProps.put("user", CONNECTION_USER);
        connectionProps.put("password", CONNECTION_PASSWORD);
        try {
            conn = DriverManager.getConnection(CONNECTION_URL, connectionProps);

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    /**
     * геттер conn
     * @return возвращает синглтон экземпляра Connection
     */
    private static Connection getConn() {
        if (conn == null) {
            createConn();
        }
        return conn;
    }

}
