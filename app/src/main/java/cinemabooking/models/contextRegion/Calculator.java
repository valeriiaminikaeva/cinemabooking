package cinemabooking.models.contextRegion;
import cinemabooking.models.clientRegion;
import cinemabooking.models.movieOnDateInTimeRegion;
public class Calculator {
    private Context context;

    public Calculator(Context context) {
        this.context = context;
    }

    public void addDiscount() {
        StudentDiscount studentDiscount = new StudentDiscount();
        if(this.context.client.getIsStudent()){
            this.context.setDiscounts(studentDiscount);
        }
    }

    public double priceWithoutDiscount() {
        double priceWD = this.context.movie.price;
        return priceWD;
    }

    public double applyDiscount() {
        ArrayList<Discount> discounts = this.context.getDiscounts();
        double price = this.priceWithoutDiscount();

        foreach (Discount discount : this.discounts) {
            if(discounts instanceof StudentDiscount && this.context.client.isStudent) {
                price *= (1 - discounts.getValue());
            }
            return price;
        }

    }
}
