package cinemabooking.models.contextRegion;
import cinemabooking.models.clientRegion;
import cinemabooking.models.movieOnDateInTimeRegion;
import java.util.ArrayList;

public class Context {
    private Client client;
    private Movie movie;
    private ArrayList<Discount> discounts;

    public Context(Client client, Movie movie, ArrayList<Discount> discounts) {
        this.client = client;
        this.movie = movie;
        this.discounts = discounts;
    }

    public Client getClient() {
        return this.client;
    }

    public Movie getMovie() {
        return this.movie;
    }

    public ArrayList<Discount> getDiscounts() {
        return this.discounts;
    }

    public void setDiscounts(Discount discount) {
        this.discounts.add(discount);
    }

    public String discountsToString() {
        String allDiscountsAvailable = "";
        for (Discount discount: this.discounts) {
            allDiscountsAvailable += discount;
            allDiscountsAvailable += " ";
        }
        return allDiscountsAvailable;
    }

    public String toString(){
        return "Client id: " + this.client.getId() +
                "\nMovie: " + this.movie +
                "\nDiscounts: " + this.discountsToString();
    }

}