package cinemabooking.models.contextRegion;

import java.util.ArrayList;

public class StudentDiscount extends Discount {

    private final static double DISCOUNT_AMOUNT = 20;

    public String toString(){
        return "Скидка составляет: " + this.DISCOUNT_AMOUNT + "%";
    }
}
