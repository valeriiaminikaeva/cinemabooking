package cinemabooking.models.movieOnDateInTimeRegion;

import java.util.Arrays;

public class Movie {
    public String name;
    public String description;
    public String genre;
    public int minAge;
    public int minutes;
    private TimeSlot[] timeSlots;
    public double price;

    public Movie(String name, String description, String genre, int minAge, int minutes, TimeSlot[] timeSlots, double price){
        this.name = name;
        this.description = description;
        this.genre = genre;
        this.minAge = minAge;
        this.minutes = minutes;
        this.timeSlots = timeSlots;
        this.price = price;
    }

    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }

    public String getDescription(){
        return this.description;
    }
    public void setDescription(String description){
        this.description = description;
    }

    public String getGenre(){
        return this.genre;
    }
    public void setGenre(String genre){
        this.genre = genre;
    }

    public int getMinAge(){
        return this.minAge;
    }
    public void setMinAge(int minAge){
        this.minAge = minAge;
    }

    public int getMinutes(){
        return this.minutes;
    }
    public void setMinutes(int minutes){
        this.minutes = minutes;
    }

    public TimeSlot[] getTimeSlots(){
        return this.timeSlots;
    }
    public void setTimeSlots(TimeSlot[] timeSlots){
        this.timeSlots = timeSlots;
    }

    public double getPrice(){
        return this.price;
    }
    public void setPrice(double price){
        this.price = price;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "movie='" + name + '\'' +
                ", description='" + description + '\'' +
                ", genre='" + genre + '\'' +
                ", minAge=" + minAge +
                ", minutes=" + minutes +
                ", timeSlots=" + Arrays.toString(timeSlots) +
                ", price=" + price +
                '}';
    }
}
