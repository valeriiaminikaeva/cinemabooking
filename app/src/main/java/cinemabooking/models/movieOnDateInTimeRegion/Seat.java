package cinemabooking.models.movieOnDateInTimeRegion;

public class Seat {
    private boolean booked;
    private double price;

    public Seat(Boolean booked, double price){
        this.booked = booked;
        this.price = price;
    }

    public boolean getBooked(){
        return this.booked;
    }

    public double getPrice(){
        return this.price;
    }

    public void setBooked(Boolean booked){
        this.booked = booked;
    }

    public void setPrice(double price){
        this.price = price;
    }

    public String toString(){
        return "Booking status: " + this.booked + "\nPrice: " + this.price;
    }
}
