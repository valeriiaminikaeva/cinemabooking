package cinemabooking.models.movieOnDateInTimeRegion;

import cinemabooking.clientRegion.Ticket;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TimeSlot{
    public Movie movie;
    public Hall hall;
    public int duration;
    public ArrayList<Ticket> tickets;
    public Date timeStart;
    public final double MORNING_PRICE_MULTIPLIER = 0.7;
    public final double NIGHT_PRICE_MULTIPLIER = 1.5;

    public TimeSlot(Movie movie, Hall hall, int duration, ArrayList<Ticket> tickets, Date timeStart){
        this.movie = movie;
        this.hall = hall;
        this.duration = duration;
        this.tickets = tickets;
        this.timeStart = timeStart;
    }

    public ArrayList<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(ArrayList<Ticket> tickets) {
        this.tickets = tickets;
    }

    public Date getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(Date timeStart) {
        this.timeStart = timeStart;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}

