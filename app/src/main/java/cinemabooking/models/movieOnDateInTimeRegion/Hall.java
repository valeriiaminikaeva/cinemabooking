package cinemabooking.models.movieOnDateInTimeRegion;

import java.util.ArrayList;

public class Hall {
    private String name;
    private ArrayList<Seat> seats;

    public Hall(String name, ArrayList<Seat> seats){
        this.name = name;
        this.seats = seats;
    }

    public String getName(){
        return this.name;
    }

    public ArrayList<Seat> getSeats(){
        return this.seats;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setSeats(ArrayList<Seat> seats){
        this.seats = seats;
    }

    public int checkFreeSeats(){
        int freeSeats = 0;
        for (Seat seat: this.seats){
            if(seat.getBooked() == false){
                freeSeats += 1;
            }
        }
        return freeSeats;
    }
    public String toString(){
        return "Hall name: " + this.name +
                "\nSize: " + this.seats.size() +
                "\nFree places: " + checkFreeSeats();
    }
}
