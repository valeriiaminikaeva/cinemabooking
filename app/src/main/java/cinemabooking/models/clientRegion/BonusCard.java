package cinemabooking.models.clientRegion;

public class BonusCard {
    private int id;
    private int clientId;
    private int number;
    private double balance;

    public BonusCard(int id, int clientId,int number,double balance){
        this.id = id;
        this.clientId = clientId;
        this.number = number;
        this.balance = balance;
    }

    public int getId(){
        return this.id;
    }

    public int getClientId(){
        return this.clientId;
    }

    public int getNumber(){
        return this.number;
    }

    public double getBalance(){
        return this.balance;
    }

    public void setClientId(int clientId){
        this.clientId = clientId;
    }

    public void setNumber(int number){
        this.number = number;
    }

    public void setBalance(double banalnce){
        this.balance = balance;
    }



    public String toString(){
        return "Id: " + this.id + " Client Id: " + this.clientId + "\n" +
               "Number: " + this.number + " Balance: " + this.balance;
    }
}
