package cinemabooking.models.clientRegion;

public class Ticket {
    private int id;
    private int clientId;
    private int timeslotId;
    private int seatId;

    public int getId(){
        return this.id;
    }

    public int getClientId(){
        return this.clientId;
    }

    public int getTimeslotId(){
        return this.timeslotId;
    }

    public int getSeatId(){
        return this.seatId;
    }

    public void setTimeslotId(int timeslotId){
        this.timeslotId = timeslotId;
    }

    public void setSeatId(int seatId){
        this.seatId = seatId;
    }

    public String toString(){
        return "Id: " + this.id + " Client Id: " + this.clientId + "\n" +
               "TimeslotId: " + this.timeslotId + " SeatId: " + this.seatId;
    }
}
