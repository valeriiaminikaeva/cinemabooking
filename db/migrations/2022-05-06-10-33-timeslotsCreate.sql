create table timeslots(
  id int generated always as identity primary key,
  hall_id int references halls,
  movie_id int references movies,
  time_start timestamp,
  time_finish timestamp
);
