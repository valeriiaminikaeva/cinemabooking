
create table movies(
  id int generated always as identity primary key,
  name text not null unique,
  description text not null,
  genre text not null,
  min_age int not null,
  minutes int not null,
  price numeric not null
);
