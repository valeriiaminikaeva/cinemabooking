create table tickets(
	id int generated always as identity primary key,
    client_id int references clients,
	timeslot_id int references timeslots,
	seat_id int references seats,
    unique (client_id, seat_id)
);
