
create table seats
(
    id int generated always as identity primary key,
    hall_id int references halls,
    row_num int not null,
     num int not null
);
