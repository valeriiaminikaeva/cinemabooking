
create table halls
(
    id int generated always as identity primary key,
    name text not null unique,
    check (name != '')
);
