create table bonus_cards(
	id int generated always as identity primary key,
	client_id int references clients,
	number int not null,
	balance numeric not null,
	unique (client_id)
);
