create table clients(
	id int generated always as identity primary key,
	mail text not null unique,
	birth date not null,
	phone text not null unique,
	name text not null,
	is_student boolean
);
